## Cloud Usage Monitor

is the mechanisms used to monitor the performance and health state of a cloud system.
We use different techniques:
- Monitor Agent
- Resource agent
- Polling agent

### Monitor Agents

We have an intermediary (monitor agent), essentially analize the traffic, so the monitor agent can collect different metrics: number of messages, size of message ecc.
Send the metrics into a log database and data (message) to the cloud service.
The drawback:
Is a one-way mechanism.
So all messages goes throw the monitor agents.

### Resource Agent / Push model

We have a resource agent that actively monitor resources (as vm) it's based on an event-driven interactions model.

When an event occur the resource agent store the collected metrics into the log database. Collect for example the 5 second average of the use of resource, the min and the max.

#### Example 

- Example 1 (**usage event**): 
    - An increase in usage is detected
    - The collected usage data is stored in log database
- Example 2 (**time event**):
  - The resource agent store the average value of the received data each 5 second.
- Example 3 (**Scaling events**):
  - The resource agent receives an event from the underlying resource management program, notify the virtual server is being scaled up
  - Metrics are stored

The drawback: if the resource agent is overloaded and for many reason we cannot get data or data are delayed we have an incorrect view  

### Polling agents

Polling agents periodically monitor the resource usage. 
While in the **Resource agents** is the resource that send data to resource agent, in the polling agents that ask data periodically to the resources to produce metrics.

For example every second ask the cpu usage and then store it into the log database, or we can store only changes. 


## Monitoring in AWS: Cloud Watch
Is based on the push model (in theory).
Is important because allows us to Enable scaling, enabling billing of the resources, Software as services etc...

Use a publish/subscribe mechanism
Cloud watch subscribe to a specific data sources
Data source == (incoming network traffic, cpu usage)
The resource publish data .

We can generate allarms
Allarms is a condition set on the value of a metric we collect, if the value is over of the threshold set for 3 second (example) we can trigger an action for example autoscaling, email notification etc.


### Cloudwatch main concepts
**Namespace** - a container for metric -> EC2, EBS.

(overlap of terminology with Docker container and linux namespace but here with a different meaning).

Space for a specific metric, 
A metric is a time oriented set of datapoints published to cloudwatch.
Metric expire after a period between 3h and 15 months, you can't change the value of the metrics for security and forensic point of view. 

For a given namespace we have a **Dimension** is a label a name/value pair for any metrics, that uniquely identify a metric.

Up to 10 dimensions can be assigned to a metric.
The same metric can be labeled with different dimension; each pair metric/dimensions is considered a different metric.

**Alarms** is defined for a single metric over a specified time period.
You can use it to trigger multiple action.
Can performe on or more specified actions, based on the value of the metric relative to a threshold over time. The action is a notification sent to an Amazon SNS topic or an Auto Scaling policy.


**SNS - Simple Notification Service**
As consequence of an Alarms you want generate a message, the information related to the allarms, so the metric and the value and id of the allarm are sent to the SNS service and THE SNS depend on your configuration, for example throw a Lambda, HTTP or SMS.

**Pay-per-use monitor**
Cloud watch is used to monitoring the resource usage

In general the monitor of the cost is called Pay per use monitor.

It can measure the quantity of the request/response, bandwith consuption and so on.

The data collected by the pay-per-use monitor is processed by a billing managment system that calculates the payment fees to generate the billing to the customer.

 

## Load Balancer

We are in a distributed computing enviroment with potentially many instances, and we want distribute the load among all instances.


The distribution of the load is called load balancing, the goal is to equally more or less distribute the workload among the many instances allocated.


For load balacing there are different policy classified in 4 categories.

1. **Content-blind Distribution**
   The widely used for example round robin, we have a set of instance of a service and you assign one request to first instance and the second to the second and so on.
2. **Asymmetric Distribution**
  You could have computing resource with different capacity you can assign more load to a powerful resource and less to a less power.

  You can use Round Robin weighted, you can assign a weight to an instance, for example in the case we have a instance **a** and another powerfull instance **b** where the power of b is = 2***a** 
3. **Workload Prioritazion**
   You can have different user priority for example: Premium user, comodity user or standard user.
   On the basis of this priority you want distribute the workload. 
   Schedule first the request or allocate more resources for High priority user.
   And you manage it with the Round Robin.
4. **Content-Aware Distribution**
   You can distribute the load on the basis of the content of the request.
   For example you could have high cpu or memory request.
   So you can assign the request in the way of you can balance the load.


#### Load balancer -- layer 4 vs layer 7

##### Layer 4
All messages from Web client arrive to the layer-4 (load balancer) and then to the target web server.

In a layer 4, one request from a client is forwarded to the target server 1 and second request (by the same web client) can be served by a different server.

**stateless** (the loadbalancer recognize the request belong the same session)

##### layer 7 or application level load balancer

The web client enstablish the connection to the Load balancer and then the load balancer intercept all request to a specific web client and 

(sticky session load balancer, so the load balancer can take trace of the request)


#### One way vs Two way
In two-way both incoming and outgoing request goes throw the same component (load balancer) and go back to the client. And this is a bottleneck.

With one-way we have only the incoming request goes throw the loadbalancer, and the outgoing no.

#### AWS offers

- Elastic Load Balancer
    - Classic load balancer (layer-4 and layer-7) replaced by the following:
  - Network load balancer (layer-4)
  - Application load balancer (layer-7)


##### Elastic Load Balancer (AWS) - network level
That are the feature and this is the architecture of the elastic load balancer at network level.

We have **listener & rule** defined by protocol:port 
for example all request HTTP:80 goes to one listener and HTTP:8000 goes to another listener.
Then there is the target associated with the *rule*.

**Target** 
Target is a VM which receive the traffic, that can have an ID or IP address. Can be registered with multiple *target group*.

**target group**
A set of targets, associated to a listener.

**Health  check**
of the whole load balancer (if load balancer responding or not to the request) or of the instances.

##### Elastic Load Balancer (AWS) - application level
At application level we have the same organization.
We have **listener rule** is more sophisticated. We have priority, action, optional host condition, *path condition* etc. Each rule assign a priority and we evaluate first high priority and then we go down.

The Application load balacner support the sticky session (with coockies), so redirect the request from a client to the same server.



#### Health check (application and network ELB)

**Based on ping**

HealthCheck interval: The amount of time between health checks of an individual instance



# Scalability

**Performance vs Scalability**
- **Performance**: measure how fast and efficiently a SW system can complete certain task
- **Scalability**: Measure the trend of the performance with increasing load.
  - A system is scalable if it is capable to maintain performance under increased load by adding more resources.

We can have two type of scalability **linear** and **Non-linear**:
- Linear: as we increase the load, we always find a number of machine (ex. if we double the load we double the number of machine).
- Non-linear: When we reach one point where handle a small portion of load we need to add a huge number of resources.


## Autoscaling mechanism

**Autoscaling**: We want realize scalability so adding and removing resources automatically.

- **Course grain**
  - We replicate the whole application and tipically in course grain scalability is implemented adding or removing new VMs.
  - It's the more easy way to scale an application
  - We can scale the number of VMs, container (or pods)
- **Fine grain**
  - We replicate an application component or a single functionality.
  - It's applied in the microservices architecture.
  - It's realized scaling containers.

**Automated Scaling listener**

That's a component essentially receive the request and distributed the request to instance avaiable.
In case isn't serve request at the level of performance. The event is notified and new instance is automatically added. The notification can be also an allarms in AWS.

**Dynamic Scalability Architecture** 

Predefined algoritmhs define the scaling conditions that trigger the dynamic allocation of IT resources from resource pools.


- The automated scaling listener could take decision on the basis of:
  - workload counter (number of service requests ????)
  - Perfomance counters/metric (es. CPU utilization, response time ecc)
  - and a combination of that and more complex counters/matrix
- The autoscaling algorithm determines how many additional IT resource can be dynamically provided.
- Scaling action
  - Scale-out (horizontal scaling)
  - Scale-up/down (vertical scaling)
  - Migration/relocation to on a system with more/less capacity.



### *MAPE*-cycle
MAPE stands for *Monitor-Analyze-Plan-Execute* cycle
**How it works**
(1) Cloud service consumers requests
(2) then the automated scaling listener (ASL) monitors the cloud service to determine if predefine capacity threshold are being exceeded.
(3) If the consumer requests increases, threshold are violated
(4) A scaling policy is used to determine the needed scaling actions
(5) The ASL initiates the scaling process, if needed
(6) The ASL sends a signal to the resource replication mechanism (RR)
(7) RR creates more instances of the cloud service
(8) Now it can distribute the load 

### Autoscaling algoritmhs

- **Threshold based** -- reactive
  - Threshold on workload or resource utilization
  - When a threshold is exceed the scaling action is triggered
  - Multiple threshold can be set.
  - Is heuristic and not optimal (over/under provisioning)

- **Model based** -- reactive 
  - is a mathematical model of the system
  - when is triggered the need for an autoscaling the model is executed and determinate the optimal number of resource to be added.
  - with mathematical you can find an exact solution or an approximation
  - Could be optimal
- ***Pro*active** (threshold/ model basede)
  - Proactive and reactive algoritmh can be mixed
  - ??

### Autoscaling in AWS
There are several concepts:
- **Autoscaling group**: a collection of EC2 instances that share similar characteristics and are treated as logical grouping
- **Launch configuration** - mounting external file system or install specific application component etc.. 
- **Scaling plan** (or scaling policy):
  - We can do manual Scaling
  - Maintain current instance levels at all times
  - we can scale on basis of time schedule (we know in advance during the night we process a lot of batch request)
  - scaled based on demand (dynamic scaling)
*Launch configuration* and *Scaling plan* are associated to a scaling group
A scaling group can be configured with multiple scaling policies.
**Remember** at least one for scaling out and one for scaling in.

![./awsAmazonAutoscalingLifeCycle](./images/awsAmazonAutoscalingLifeCycle.png)


We have EC2 instance attached to a scaling group
When scale out happen we move to pending state (wait and proceed).
When istance is in service if the instance fail it could be terminate.
We scale in happen we can terminating or enter in standby mode. 
When we have an instance in standby mode when we have the need to scale out we pick this istance (from the hotpull) in pending state.
We detach from a scaling group when there is a fail.


#### Autoscaling in AWS - *Dynamic scaling*
Simple scaling and step Scaling --- threshold base policy
  - you define how many instance to add/remove and when
  - When is given by a threshold on performance metric values
Taarget tracking scaling --- Model based
  - You define a desired value for a performance or workload metric e.g cpu utilization

##### Simple vs Step Scaling
Simple scaling:
- A time t1 is determine the scaling action (violation of threshold) -> ad new virtual machine. The new VM is added and ready to work at time t1'.
  t1' - t1 = **time to scale**
  There is time interval called **cold-down** in this time interval the load reach a stable a value, because we scale.
  t1'' - t1' = **cold-down**
  - This approach is a little bit slow.

Step Scaling:
- You can scale at any time t1,t2,t3 without cold-down interval.


#### AWS Scaling adjustment types
- Change in capacity (capacity = number of VMs)
  - You specify the adjustmentin capacity (+/-)
  - If the current capacity of the group is 3 instances and the adjustment is 5, then when this policy is performed, Autoscaling adds 5 instance to the group for a total of 8 instances.
- Exact capacity
  - You specify the new capacity of the scaling group
  - If the current capcity of the group is 3 instances and the adjustment is 5, then when this policy is performed, Auto Scaling changes the capacity to 5 instances (and not 3+5 as before).


##### Note
In all the scaling policies must be define the maximum and minimum scaling group size.

A scaling adjustment cannot change the capacity of the group above the maximum group size or below the minimum group size.

Why?

#### An example for percent Capacity

Scale out policy is based on 3 threshold. If the value is in between the range 50 60 we don't add vm
If the value is between 60 and 70% we add 10 vm.
We can define also the scale in policy in which we decine how many vm remove.
![](./images/percentileCapacity.png)



#### Aggregate Metrics


At a given instant of time *t* the "***Performance metric value***" used by scaling policiy is the aggregated value of it collected from all the instances for that metric.
For example we can use the average as aggregation function; there is 5 instances the metric is cpu utilization we calculate their mean.

#### WARM UP
Number of seconds that it takes for a newly launched instance to work at its full capacity is called **warm-up**. 
**Until its specified warm-up time has expired**, an instance is not counted toward the aggregated metrics of the Autoscaling group.

#### ALLARMS

![](./images/allarms.png)

The allarm occurent whena a matric value is upper/lower a desired threshold for a specific time period.

![](./images/stepScalingMechanism.png)

![](./images/mapeKCycleAWS.png)
**Monitoring** and **analyze** is performed by Cloud watch
**Analyze** phase checking the allarms.
The **planning** is done by one of the algoritmh of Dynamic scaling 
**Execute** is done with CLI, or internal mechanism (as launch instance,  attach instance to autoscaling group etc.)



### Autoscaling in Containers
We have talked about the autoscaling in vm, in container is similar.

**SWARM** allows to maintain a certain capacity (number of container)
  - Other compontes are needed to enable Autoscaling
    - Orchestration and monitoring, e.g Kubernetes + cAdvisor
  - we can change capacity manually
**Ocherstration**:
  - Allow to setup infrostruction to manage container.
  - Container orchestration allows cloud and application providers to define how to select, to deploy, to monitor and to dynamically control the configuration of multicontainer packaged applications in the cloud 

**Kubernetes**:
With kubernetes we can change capacity automatically of swarm, but in k8s we don't have swarm concept, we have pods.

- **Service discovery** and **load balancing**:
  - Expose a container using the DNS name or using their own IP address.
  - To load balance and distribute the network traffic so that the deployment is stable.
- **Storage orchestration**
  - To automatically mount a storage system of you choice, such as local storages, public cloud providers, and more.
- **Automated rollouts and rollbacks**
  - To describe the desired state for deployed containers
    - it can change the actual state to the desired state at a controlled rate.
  - For example you can automate K8S to create new containers for you deployment, remove exist
- **Automatic bin packing**

- **Self-healing**

- **Secret and configuration management**



### Kubernetes components
Essentially k8s has two manager **kube**-controller-manager and **cloud**-controller-manager.

The **cloud**-controller-manager allows to interact with external cloud services.
The kube controller manager allows to controll the ***kubernetes Nodes***













































































