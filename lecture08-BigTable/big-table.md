# Big Table

Two types of database:

- Relational DBMS, guarantees atomicity, consistency, isolation and durabiltity (ACID), **usually aren't scalable**
- NoSQL DBMS, data without any kind of structure, the may not guarantee **ACID property**
  - key-value store, where data are organized in table and the data are stored used key-value

## Transaction processing 

Many cloud app are based on **online transaction processing (OLTP)** , we operate under tight latency constraints.

The goal of OLTP is to reduce the response time, memcaching is a solution, but there is a tradeoff related to the increase of user, hence we need to scale.

Horizzontal scaling with database is an issue, moreover with the relational db, since whether we have replica of the same db we must guarantee the consistency, and it's too hard.

## Big table

It was designed to support many Google products such as google search engine, gmail...

It achives scalability, high perfomance and high availability.

Variety of workload:

- Throughput-oriented batch-processing jobs;
- Latency-sensitive servign of data to end users

Big tables allow us:

- to control dynamically the layaout and format of the data
- the locality of the data, we can control the physical node where the data are stored

> Each data is **unique** indentified with the name of the **row** and the name of the **column**, moreover we also have the time which allow us to manage the version of the data

- Data are stored and treated as **uninterpreted strings**, it's a deal of the client to give the right rappresentation of this data

3 dimensions: rows, columns and timestamp(third dim)

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture08-BigTable/img/big-table.png" alt="slides11" style="zoom:50%;" />

In the example above, for each rows we have a web page URL, we have a column for the html content, which have different version.

## Row

**Row key up to 64KB** (10-100 byte is the typical size)

Table-rows are ordered lexicographically

Given a table and given a ranges of row, we can partition it, the partition is called **tablets**, thus we could distribute the load of a single table among many nodes. This strategies let us to handle huge amount of rows in high-capable way.

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture08-BigTable/img/big-table-01.png" alt="slides11" style="zoom:50%;" />

## Column

- Column key are grouped into sets called **column families**, is a way to group key for the column
- A column key is composed by a prefix called **familiy**, and a unique part which is the **qualifier**
- All columns which have the same **family**, they share the same type
- A family must be created before storing data under any columns key
- The number of family should be small up to 100
- Families should rarely cange
- Number of column in a family unbounded

> Number of column = the sum over all (families x the keys qualifier per this specific family)

## Timestamps

Each cell can contain multiple version of the same data

- Version are indexed by timestamp (64-bit integers)
- Versions are stored in decresing timestamp order
- The most recent version can be read first

Timestamp assignment 

- by BigTable, represent real time in **microseconds**
- **by client app; collision must be avoided by generating unique timestamps**

Two mechanism to garbage collect cell versions

- only the last n versions of a cell be kept
- only new-enough versions be kept (e.g. latest 7 days )

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture08-BigTable/img/big-table-02.png" alt="slides11" style="zoom:50%;" />

Example of Gmail BigTable implementation

## Google SSTable

The format used to store the BigTable data is SSTable. Each SSTable contains a sequence of blocks and a block index to loacete blocks.

To access an SSTable block:

- The index is loaded into memory whe. the SSTable is opened;
- The bock is found using binary search in memory;
- The block is access with a single seek on disk

An SSTable can be also loaded all in memory

## BigTable architecture

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture08-BigTable/img/big-table-03.png" alt="slides11" style="zoom:50%;" />

Consist of one master server and many tablet servers. A BigTable cluster store of a number of tables; each table consist of a set of tablets.

The master is responsible for:

- Assigning tablets to tablet servers
- Detecting the addition and expiration of tablet servers
- Balancing tablet-server load, and garabage collection of files in GFS
- Handling schema changes such as table and column famility creations

Tablet server is mainly responsibile to manage a set of tablets. **When the tablet grows in size, the tablet server is responsible to split tablet that are increased too much**.

> For each tablet there is only one tablet server

### Chubby

Provides a lock mechanism based on files and directories, is used for:

- To ensure that there is at most one active master at any time;
- To store the bootstrap localtion of BigTable data
- To discover tablet servers and finalize tablet server deaths
- To store BigTable schema information
- To store access control lists