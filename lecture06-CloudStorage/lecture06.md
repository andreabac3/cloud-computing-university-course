# Cloud Storage

System used in the cloud to store large amount of data.

- The data can be stored in different way in cloud systems.
    - Distributed file systems
        - Google File System (GFS), Hadoop Distributed File System (**HDFS**)
        - GFS e HDFS rely on concept early developed in Network File Systems and Parallel File Systems.
    - NoSQL database
        - E.g. Cassandra, MongoDB
    - Key-value storage Systems
        - Specific family of NoSQL
        - E.g Big Table, Dynamo
- Common goals
    - Massive scaling on demand 
        - We the request of storing file increase, we enlarge capacity ondemand
    - High availability
    - Simplified application development and Deployment

## Atomicity

Atomicity is important in a multiple users system.

- Multi-step operation (**transaction**) should complete without any interruption (**atomic**).
    - For example, we you reserve a sit on a flight.
    - Bank transaction.

- **Atomicity requires HW support**
    - *Test and Set*: Writes to a memory loc. and returns the old content of that memory cell and doing that as non-interruptible operation.
    - *Compare-and-swap*: Compares the contents of a memory loc. to a given value and, only if the two values are the same, modifies the contents of that memory loc to a given new value.

- Atomicity require mechanism to access shared resources
    - **Locks, semaphore, monitors**
    - Allow to create critical section

- Two type
    - all-or-nothing
    - before-or-after

#### All-or-nothing - Atomicity

![all-or-nothing](./images/all-or-nothing-atomicity.png)

- Two phases
    - **pre-commit phase**
        - allocation of resources, fetching data, ...
        - is a preparatory actions, can be undone (can be abort)
            - can abort if we cannot allocate all resources needed
    - **commit-point**
        - when we have all resources, we can procede with the transaction we reach the commit point.
    - **Post-commit phase** - Irreversible actions
        - alteration of the only copy of an object.

To manage failures and ensure consistency we need to maintain the history of all the activities. Logs (or *journals*) are necessary. In order to rollback, or if a failure happen you can rollback.

#### Before-or-after - Atomicity

![all-or-nothing](./images/before-or-after-atomicity.png)

**Definition**: *before-or-after atomicity should grant the order in which you do Read and Write do not influence the operations result.*

Concurrent actions have the before-or-after property
if the effect of the action, is independent from the order in which the action are performed, in the point of view of the action invokers

For example, we have three processes (A, B, C) as shown in the figure.

If A read and the B read/write and then A write is the same of A R/W and B R/W.

### Storage models & desidered properties

- Desired properties
    - read/write coherence
    - before-or-after Atomicity
    - All-or-nothing Atomicity

Read/Write coherence:
    - the result of a read of memory cell M should be the same *as the most recent write* to that cell.
    - You cannot read a old value if a write is performed in the middle between the last write and the current read.
![all-or-nothing](./images/rw-coherence.PNG)

- In the cell storage model we assume:
    - // cell storage model is characterized by r/w coherence
    - we assume that all cells have the same size. (because all sector has the same size)
    - each object fits exactly in one cell
    - Read/Write unit is a sector or block 

### Journal Storage model
The cell storage model evolve into the Journal Storage models.
A model for storing complex objects like records consisting of multiple fiedls.

A manager + cell storage
- The entire history history of a variable (not only the current variable) is maintained in the cell storage.
- No direct access to the cell storage; the user sends requests to the journal manager.
- The journal manager translates user requests to commands sent to the cell storage:

# High performance file systems

The network File System is based on client server model
the interaction among the different node use the remote procedure call
Each node has it's local file system, (lookup for a file) when an application ask to access to a file. First you should check if the file is in the local FS, if the file isn't in the local FS then request to Vnode layer (virtual node layer) that lookup in the local table where is located the file, so all other nodes part of the network FS. 
So all nodes know the precence of the other nodes (throw the communication network).
The single point of failure is if the "local FS" fail  all other remote host lost the information about the file in this fs.
![networkfilesystem.png](./images/networkfilesystem.png)

![UnixFilesSystemRPC.png](./images/UnixFilesSystemRPC.png)

![UnixFilesSystemRPC.png](./images/UnixFilesSystemRPC-2.png)



## General Parallel File System (GPFS)

So because the network file system don't scale, we have invented the Parallel FS.
The idea is connect thousand of nodes in a way we can distributed the storage, and multiple application can access concurrent to the same file.

Max FS capacity is 4PetaByte so 4096 disk of 1TB each

SAN (Storage Area Network)
SAN tipically are used as the infostructure to implement the Parallel FS.
Has journaling, logs maintained separately by each I/O node for each file system mounted.  

The Parallel FS introduce the concept of redundancy of the data so file and metadata are stored in two or more hdd.

The design of the FS allow the concurrent access, with a locks guarantee consistency 
Is hierarchical because we have a central lock manager e a local lock manager.


![GPFS.png](./images/GPFS.png)


# Google File System (GFS)

Designed based on: 
1) High reliability to hardware/software/human/application failures
2) FS designed for the cloud: a careful analysis of the file characteristics 
3) the access models of that file



## GFS: cloud access model 


Most important features of the "cloud access model:
- files size range from a few GB to 1000 TB for a single file
- How the file are accesses
    - Nobody do a random write into the file but most of the time people add lines at the end of the file
    - Also the read: usually application do the sequential read of the file
- Response time is not the main requirement; data are processed in bulk.
- The consistency model is relaxed in order to simplify the system implementation 

## Files in the GFS

**What is a file in the GFS?**
A file is a collections of fixed-size segment called chunks
- **chunk size 64mb**; normal fs work with blocks of 0.5-2MB
- Stored on Linux File System
- Each chunck is replicated three times (in different server)

**Why a big chunk file?**
To optimize performance for large file and to reduce the amount of metadata maintained by the file system.

To increase the likelihood that multiple operations will be directed to the same chunk

To reduces the number of requestes to locate the chunk;

To maintain a persistent network connection with the server, instead of looking for each mini chunk stored in different server.
To reduce the disk fragmentation;

## GFS Cluster architecture
![GFS_cluster_architecture](./images/GFS_cluster_architecture.png)

We have a master node that controll several chunk server and master mantain information like filename, access controll, location of the chuck of one file, free spaces in the chunk server etc.

The master is the only one know the chunk server.
Master work in main memory (ram) to be more efficient.
Master never access to the disk. So all is in the main memory.

The reliability is the main concept, each operations is stored into log (journal).
All changes in the chunk are stored in the log.

In case of failure the master can reply the log and can rebuild the state of the chunck server.

To minimize the recovery time, the master periodically checkpoints its state and at recovery time replays only the log records after the last checkpoints.

## GFS File access
How files are accessed?

**The file access is handled by the application and chunck server**

the master grants a lease to a chunck server.
The **primary** chunk server is responsible to handle the update

**but the creation of the file is handled by the master**
if you want access to file to read so  the Master is contacted the lease to a chunck server is given and then the application access directly to the chunck server.

## GFS write on a files
What happens when we write to a file 
![gfs_write_on_file](./images/gfs_write_on_file.png) (52:40)

# Hadoop Distributed File System (HDFS)
![HadoopDistributedFileSystem.png](./images/HadoopDistributedFileSystem.png)


Hadoop (Apache Hadoop) and the HDFS are two different things.

- Apache Hadoop is software system to support processing of extremely large volumes of data (big data applications)
    - MapReduce + datastore (HDFS, Amazon S3, CloudStore, ...)
- HDFS is distributed fs written in Java
    




