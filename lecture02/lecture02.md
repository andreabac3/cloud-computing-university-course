# Cloud Computing

## Business drivers

### Capacity Planning

Capacity planning is related to provide the right amount of capacity when needed 
>  To avoid Overprovisioning and Underprovisioning

### **Different strategy**

Lag strategy (reactive)
> adding capacity when the resources are full.

Lead strategy (proactive)
> Adding capacity resources in anticipation of demand 

Match strategy (proactive)
> Adding resources capacity in smalls increment, as demand increases 

### **Cost reduction**
Problem
> A company should expand its IT capacity to cope with the increasing workload demand

On-premise
> Up-front cost 1M EUR
> Annual Operational overhead 2M EUR

Cloud Solution
> Cloudification/migration 1M EUR
> Annual cost of cloud resources 1.2M EUR

### **Organization agility**
Measure of an organization's responsiveness to change.
The response some time should be in hours or few days.
On-Premise -> One month (ordering, installing)
Cloud sol -> Minutes, days

-----------------
## **Defining Cloud Computing**
In 2012-2012 was a really a buzzword
> Like AI and ML today

The term (and the symbol) *cloud* historically used in the telecommunications industry.
> It became the symbol for the Internet.

Cloud computing, an **Internet-centric** way of computing.
> The internet plays a fundamental role in cloud computing
> Services are delivered and made accessible through the internet.

### The NIST Definition
NIST: National Istitut of Standard Technologies
*Cloud computing is a model for enabling ubiquitous, convenient, **on-demand network access** to a shared pool of configurable computing resources (e.g. netowrks servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction* <br>
**On-demand** 
deliver services with a given pricing model -> most cases a "pay-per-use" strategy<br>
**Network access** 
Recall the concept of *internet-centric* way of computing
It makes it possible to access online storage, rent virtual hardware, or use development platforms and pay only for their effective usage, with no or minimal up-front costs.

### Services In Cloud Computing Style
Three criteria to discriminate whetere a service is delivered in the cloud computing style:

 1. The services is accessible via a Web browser or a Application Programming Interface (API)
 2. Zero capital expenditure is necessary to get started 
	 > no human expertise, no buy hardware)

 3. You pay only for what you use as you use it

------------------

## Essential Characteristic (NIST)
This cloud model is composed of five essential characteristics, three service model, and four deployment models.
**Characteristics**
 - On-demand self-service
 - Broad network access
 - Resource pooling
 - Rapid Elasticity
 - Measured Services
 
 introduce new concept (elasticity, multitenancy, monitoring) 
 Enable by technologies (broadband, cluster computing, virtualization, ...)
**Service models**
 - SaaS (Software as a Service)
 - PaaS (Platform as a Service)
 - Iaas (Infostructure as a Service)

**Deployment models**
Private, Community, Public, Hybrid cloud.

### On-demand self-service
Rely On
- Orchestration technologies (e.g. Kubernetes)
- Interfaces
	- Web interfaces (Openstack, AWS, Google Cloud)
	- Dedicated shell (e.g AWS Shell)
	- Programming API (e.g- AWS SDK for Java, Python, .Net, Ruby, Go, ...)

### Broad network access
Access cloud resources from internet using different devices.
Rely on broadband network and internet technologies (enabling technologies)
(performance and security issue are in the network and at the edge)

### Resource pooling 
from the NIST definition
Computing resources are pooled to serve multiple consumers usign a **multi-tenant** model

 - location independence, no control or knowledge, maybe be able to specify location at a higher leel of abstraction (e.g. country, state, or datacenter)
 - Different physicial and virtual resources dynamically assigned and reassigned according to consumer demand

### Multi-tenancy (of application)

 - Multiple users (tenants) access the same application logic simultaneously
 - Each tenant has its own view of the application as a dedicated istance and is unaware of other tenants. Applications have isolated data and configuration informations.
 - Enabled by virtualization and distributed software technologies.

#### Characteristics of multitenant applications
An example - [Google Classroom](https://classroom.google.com/) (Saas) 
Customer: Sapienza, Dipartimento Informatica
Users: Teachers, Students.

 - Usage Isolation & Data Security
	 - How Sapienza's teachers and students use it, doesn't effect other universities/coursers.
	 - cannot access data belongs other university.
 - Recovery
	 - Backup and restore procedures for Sapienza and other university are separately executed.
 - Application Upgrades
	- Are transparent for each tenant
-  Scalability 

- Metered Usage

- Data Tier isolation 


#### What is the difference between multi-tenancy and virtualization

##### Virtualization
	- Multiple virtual copies of the server env (OS + applications)
	- Is the physical resource that is shared
##### Multitenancy
	- An application is designed to allow usage by multiple different users.
	- The application, use the virtualization, can run on one or more VM or physical server.

##### Virtualization vs Multitenancy
###### Virtualization without Multi-tenancy
One user: App1 + App2 / VM / HW <br>
We have a phisical server, a VM on top of the hw, we have two different application, in that case we have virtualization and we haven't the multi-tenancy app.

###### Multi-tenancy on the HW
Two user: (App1 + App2 / VM1) + (App1 + App2 / VM2) / HW <br>
We have a phisical server, a vm on top of the hw, two different vm in which there is a replica set of application 

###### Multi-tenancy at application level
Virtualized env on top of the HW an application realised as multi-tenancy app, different thread, each user has it's own thread. Each user is isolated. 
Three User: Trd1 Trd2 Trd3 / app1 / vm / hw


#### Rapid Elasticity
Let us consider a IaaS provider, AWS or Google<br>
You can create VMs manually
	- You can manually add/remove VMs (EC2 instance), that is you can scale
What are the drawback?
	- How if in 1 min. the workload increase of 200%
	- How you can recognize that situation
	- How you can react properly
You need some automatism
 - monitor VMs CPU usage (continuously)
 - if CPU usage > 70% for 1 min. add 1 VM
That is the auto-scaling and elastic load balancing

##### Rapid Elasticity (Scalability)
###### Defintion
Is the degree to which a system is able to adapt to workload changes by provisioning and deprovisioning resources in an automatic manner, such that at each point in time the available resources match the current demand as closely as possible.
Elasticity is related to the concept of Scalability
###### Two way to do scalability:
	- Horizontal Scaling  
		- Scaling out (add instances)
		- Scaling in (remove instances)
		- Add extra instances 
	- Vertical Scaling
		- Scaling up (Add CPU, memory) 
		- Scaling down (Remove cpu, memory)

##### Performance vs Scalability
- Performance measure how fast and efficiently a system can complete certain tasks.
- Scalability measure the trend of performance with increasing load.

##### Measured Service
We usualy pay for a used resources as VM on the basis of the use or MB used or I/O transfer and so on.
Cloud try to measure the time, resources we are consuming, the cloud try to 

To guarranty the performance and the 

###### Measured Service (cont'd)
- Consumers need to monitor the usage of their resources to aumate management task.
	- Scaling (autoscaling thresholds)
	- Controlling costs (alerts)
	- Guarantee application specific SLA (throughout, response time, ...)
	- Increase resiliency

- Resiliency
	The ability to provide and maintain an acceptable level of service in the face of faults and challenges to normal operations

- E.g.
	- AWS Elastic load balancer
		- if a node is down automaticallly is excluded by the pool 
	- AWS Autoscaling
		- You can configure the minimum number of servers you want always up despite failures
	- Docker container orchestration (e.g Kubernetes, Swarm)
		- You can define the desired number of container always up and running despite failures.



## Service Models
Infrastructure as a Service 
	- Service provider: Virtualization, server, Storage, Networking
Platform as a Service <br>
	- Service provider: Runtime, Middleware, OS, Virtualization, server, Storage, Networking
Software as a Service <br>
	- Service provider: Applications, Data, Runtime, Middleware, OS, Virtualization, server, Storage, Networking

The subscriber are responsible of all blue boxes.
The service is responsible of all gray boxes.

![Service Models](./images/service_models.png)

- Limit the control of the cloud consumer (subscriber)
- Separate the responsabilities of the cloud consumer and cloud provider (service provider)
- ** Moving from IaaS to SaaS ** 
	- The control of the consumer on the resources decrease
	- The expertise needed to manage the cloud service decrease

## Deployment models
- Public cloud 
	- Netflix, Spotify, etc.
- Private cloud
	- Any cloud dedicated to a specific customer 
	- On-premise, you relase your own datacenter and manage it by yourself.
	- Virtual Private Cloud
		- Amazon VPC
- Hybrid: private + public
- Community Cloud 
	- special form of cloud
	- Cloud infrastructure is provisioned for exclusive use by a specific community of consumers from org that have shared concerns. 
	- Example: AWS GovCloud (IaaS)


Cloud Computing roles
	























