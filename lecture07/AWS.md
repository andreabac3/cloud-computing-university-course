## Cloud usage monitor

### Monitor agents

**Foto**

All the traffic goes trhough the monitor ag. and it collects several data such as # of buckets # of messages, the size of the message, however the monitor ag. can go inside the speicifc packet. All information collected are stored in a log database in order to analyse them in the future.

Cloud consumers -> Monitor agent -> Cloud Service, this pipeline is called one way mechanism

### Resource agent 

**Foto**

It retrives all information in active way, when an event occur, the resource agent store the collected metrics in the log database, it has an amount of time which drives it to stored the information, every amount of time continuosly stores data. In general when a specific event is fired, the resource handles it.

Kind of event:

- Usage event, i.e. CPU consuming;
- time event;

- Scaling event

## Polling agents

**Foto**

Despite of Resources agent, which the data came to it, in the polling mechanism we have a retrival approach with a speicific period, it`s send a message to the cloud service to get the information and then it stores info to the db. We can either store all the values or just the changes.

## AWS CloudWatch

It's the most important AWS component, in theory is based on a push model, implemented with a publish/subscribe machanism.

The idea is that when you use CW you subrisce it to a specific data source (i.e. utilization of CPU , net trafic...)

**For each resources available in AWS you can access to a set of metrics which you can attach CW in order to log them.**  

You can set an allarm togheter a metric, which checks if a specific condition holds. 

### Main CW concepts

- Namespace, don,t confuse it with docker or linux meaning, namespace in CW enviroment is a container and a metrics is a time orientied set of datapoints published to CW, and expires after a period 3h-15 months (can't be deleted manually). 

> Each data point has a time stamp

​	