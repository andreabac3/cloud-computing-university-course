# Open Questions

## The big picture of cloud computing

---



1. **Describe and discuss what should be the characteristics of a service/application to be**
   **classified as cloud service.**

   Answer:

   A service can be described as cloud service if:

   1. It is accessible via a non-proprietary web browser or web services API
   2. There is no need for a prior capital expenditure
   3. Users only pay for what they use (pay-per-use model)

---



5. Describe and discuss the following essential characteristics of cloud computing (as**
   **defined by NIST): resource pooling; rapid elasticity; and measured services.**
   Answer:

- **Resource pooling**. The provider’s computing resources are pooled to serve multiple consumers using a multi-tenant model, with different physical and virtual resources dynamically assigned and reassigned according to consumer demand. There is a sense of location independence in that the customer generally has no control or knowledge over the exact location of the provided resources but may be able to specify location at a higher level of abstraction (e.g., country, state, or datacenter). Examples of resources include storage, processing, memory, and network bandwidth. 
- **Rapid elasticity.** Capabilities can be elastically provisioned and released, in some cases automatically, to scale rapidly outward and inward commensurate with demand. To the consumer, the capabilities available for provisioning often appear to be unlimited and can be appropriated in any quantity at any time.
-  **Measured service.** Cloud systems automatically control and optimize resource use by leveraging a metering capability1 at some level of abstraction appropriate to the type of service (e.g., storage, processing, bandwidth, and active user accounts). Resource usage can be monitored, controlled, and reported, providing transparency for both the provider and consumer of the utilized service.

---

6. See 5.

---



7. **Why the cloud service models introduce the “separation of responsibilities” between****
   **the cloud provider and the cloud consumer? Bring an example**
   Answer:
   Cloud service models introduce a separation of responsibilities with respect to on-premise services: resource owners have to manage the whole stack when it comes to on-premise models, while the different cloud services (IaaS, PaaS, and SaaS) allow to only manage a portion of it.

- IaaS leaves from networking to virtualization layers to the cloud provider, while the user has to configure layers from OS to Application layers; (i.e. AWS EC2)
- PaaS leaves from networking to runtime layers to the cloud provider, while the user has to configure Data and Application layers; (i.e. Google Cloud Dataproc)
- SaaS leaves the whole stack to the cloud provider, while the user only has to access the application. (i.e. Google Docs)

---



8. **Consider the classical computing stack in the figure and explain how the cloud service models introduce a separation of responsibility between the service provider and the service subscriber.**

- Infrastucture as a service (IaaS)
   Service provider manages from networking to virtualization layers, while the service subsricbers manage from OS to application layers; suitable for IT architects (i.e. AWS EC2).
- Platform as a service (PaaS)Service provider manages from networking to runtime layers, while the service subsribers manage data and application layers; suitable for cloud developers (i.e. Google Cloud Dataproc).
- Software as a service (SaaS)
   Cloud provider manages the whole stack, users only use the provided applications; suitable for end users (i.e. Google Documents).

## Enabling technologies

---

9. ==**Suppose that you would like to deploy a web application composed by: a frontend**==
   ==**server to receive http requests and produce http reply/answers; a business logic server**==
   ==**to build the content of the web pages; a database server, used by the business logic to**==
   ==**extract information. Suppose also that the business logic component should scale**==
   ==**(horizontally). Which architectural model could be used for the deployment? Motivate**==
   ==**your answer.**==
   Answer:
   Microservices model because allow us to: 

   - It enables the continuous delivery and deployment of large, complex applications;
   - Services are small and easily maintained;
   - Services are independently deployable;
   - Services are independently scalable;
   - The microservice architecture enables teams to be
     autonomous;
   - It allows easy experimenting and adoption of new
     technologies;
   - It has better fault isolation;

   ---

   

10. ==**Consider the inter process communication schema in the figure. The student should:**==
    ==**assign the proper names to the components with labels A,B,C,D,E,F,G and explain**==
    ==**their role/functionality.**==

    - **A.** RPC Library, is a directory with the all Remote Procedure that can be invoked in the Node A. Its function is to create a message, serialezed the parameters and call the procedure C on the Node B
    - B. Marhsaling the parameters
    - C. Unmarshaling the return value
    - D. Unmarshalling the parameters 
    - E. Mashaling the return value
    - F. RPC Service, receive the request and look into G which invokes the asked procedure. After the invocation F send back the return value.
    - G. RPC Registry, where all enables procedures  are published by the Node B(Sever)

    ---

    

11. ==**Consider the inter process communication schema in the figure. The student should:**==
    ==**explain the behavior of the communication protocol, that is the calls 1, 2, 3, 4, 5, 6**==

    Answer:

    - 1.The Node A call the procedure C which belongs to the Node B
    - 2.RPC Library, is a directory with the all Remote Procedure that can be invoked in the Node A. Its function is to create a message, serialezed the parameters and call the procedure C on the Node B
    - 3.RPC Library pass the message to the Marshalling phase of the Node B, which serialazed the parameters
    - 4.The Unmarshaling componets of the Node B deserialezed the parameters and pass the message to the RPC Service
    - 5 RPC Service interact with the RPC Registry, 
    - 6 RPC Registry run the procudure C where the return value is passed to the RPC Service

    ---

    

12. **Consider the inter process communication schema in the figure. The student should:**
    **describe the communication protocol calls 2 – 21 initiated by the “Ask for Reference”**
    **call (in the node A side).**
    https://drive.google.com/file/d/1X4r0p93JGLmg-CWGENnROl2dqryT_aKr/view

    

    ---

    

13. **Consider the Service Oriented Architecture implementation of inter process**
    **communication. Give a description of the main components in the SOA model and of**
    **the message exchange necessary to let a service consumer to use a service (offered by**
    **a service provider).**

    Answer:
    The Service Oriented Architecture (SOA) describes applications as a collection of coordinated services. Web Services is a SOA implementation of the RPC model over HTTP. A single service consists of a software component with the following characteristics:

    1. Explicit boundaries with minimal interfaces for reuse and simplied use purposes

    2. Autonomous services that can be integrated in different systems at the same time, handling their own

       failures

    3. Services share schema and contracts in order to describe the structure of messages sent/received by

       them

    4. Policy-based compatibility such that requirements are defined in terms of expressions

    There are two roles in SOA:

    1. Service publisher/provider
        Maintains the service, publishing it into a registry along with its schema and contracts
    2. Service consumer
        Locates service’s metadata in the registry, implements the required components to bind and use it

    Service providers and consumers can belong to different organizations, and components can play both roles; service orchestration is used in order to coordinate and manage the composition and interaction of services, while service choreography is the coordinated interaction of services without a single point of control.

    The service consumer discovers a service and queries the service registry for the binding information; once it obtains such information, the service consumer uses it to enstablish a connection to the service provider, and then it will be able to use the service.

    ---

    

14. **Explain the concept of autonomic computing and MAPE-K cycle**
    Answer:
    Autonomic computing is the ability of systems to manage themselves according to a high-level description of the administrator’s goals. It consists of the following properties:

    1. Self-configuration
        Ability to accomodate varying and unpredictable conditions
    2. Self-healing
        Ability to remain functioning in the event of problems
    3. Self-protection
        Ability to detect threats and take appropriate actions
    4. Self-optimization
        Ability to constantly monitor itself and take optimal actions w.r.t. the goal

    Autonomic computing is based on a central element called Autonomic manager that implements the MAPE-K cycle, consisting of:

    1. Monitoring the system’s health state
    2. Analyzing the monitored data
    3. Planning reconfiguration actions
    4. Execute the planned actions

    leveraging the Knowledge about the system throughout all the phases.

15. **Consider the three dimensions of scaling introduced by the “The scale cube” (in the figure). Which of the three dimension is enabled by the microservices and why.**

    Answer: 
    Microservice enables functional decomposition(Y-axis) decompress an application into services. Each one of the service can be scaled  horizontally or vertically or a combination of both. This apporaches allow us to scale only the functions which are really needed to scale and not the entire application.

## Virtualization

---

21. **What are the main differences between system level virtualization (a.k.a. Hypervisor) and operating system level virtualization (a.k.a. Container)? (suggestion: you can explain the differences referring to technologies you know)** 
    Answer:
    System-level virtualization requires the whole OS, libraries, and application stack to be run inside a VM, having a VMM that handles calls between VMs and hardware; OS-level virtualization using Containers allows for sharing a single OS and running libraries and applications inside an isolated environment. Containers like Docker exploit namespaces, control groups, and union filesystem.

    ---

    

22. **Consider the live migration steps in the figures. Explain in detail step 2 and step 3**
    Answer:

    ---

    1. Pre-migration
        Determines which VM has to be migrated and where
    2. Reservation
        A container is initializated in the new host
    3. Iterative pre-copy
        Stores the VM execution context, transferring it in rounds to the new host; detect when the last copy is small enough to continue
    4. Stop & copy
        Stop the VM for as short as possible, in order to copy last updates to its execution context to the new host
    5. Commitment
       VM reloads its context on the new host, connections are redirected to it from its old host
    6. Activation
        Connection to local devices 

25. **Describe all the actions executed by the docker daemon when the following commandis executed? <u>docker run -i -t ubuntu /bin/bash</u>**
    Answer:
    The Docker deamon does what follow:

1. Locate and eventually download the ubuntu image (if not locally stored)

2. Create a new containers 

3. Allocate a R/W file system as final layer of the image

4. Create a network interface connected to the default networking

5. Start the container and run /bin/bash command

   ---

29. **What are the main components of the Elastic Load Balancer? Describe their role andhow they work.**	

    Answer:
    An Elastic Load Balancer makes use of listeners, and each listener is configured with a rule, protocol and port, and targets (resources). Each target can be registered to one or more target groups: each target group is associated to a listener and contains a health check component, both for assessing instances’ and the load balancer’s health state.Listener rules consist in priority and action, and rules are evaluated by priority, from the lowest to the highest.

## Cloud Mechanisms

---



29.  **What are the main components of the Elastic Load Balancer? Describe their role andhow they work.**

    Answer:

    An Elastic Load Balancer makes use of listeners, and each listener is configured with a rule, protocol and port, and targets (resources). Each target can be registered to one or more target groups: each target group is associated to a listener and contains a health check component, both for assessing instances’ and the load balancer’s health state.Listener rules consist in priority and action, and rules are evaluated by priority, from the lowest to the highest.

    ---

    

30. **Consider the autoscaling life cycle for an EC2 instance represented in the figure.**
    **Describe all the state transitions that could occur as consequence of the occurrence of**
    **a scale-out event and that bring the instance in the “terminated” state.**
    Answer:

    ---

    

31. **Consider the autoscaling life cycle for an EC2 instance represented in the figure.**
    **Describe all the state transitions that could occur as consequence of the occurrence of**
    **an attach-instance event and that bring the instance in the “standby” state**
    Answer:
    
    ---
    
    

37. **Describe how the Step Scaling algorithm works**
    Answer:

    1. An Automated Scaling Listener (ASL) listens for alarms;
    2. In case the alarm X occurs, the ASL calls the component for that alarm, which will:
       1. execute the scaling policy for X (based on the threshold, performing its actions);
       2. wait for the warm-up period to expire;
       3. compute the aggregated metrics of the autoscaling group;
    3. In the meantime, the ASL returns listening to alarms, without waiting for the component to end its execution.	

    In the Step Scaling algorithm there is no cooldown period to wait before a new scale-out/in action can be applied again.	

    ---

    



