# Virtualization

### Why virtualization has been widely explored?

- Increase performance and computing capacity
- Underutilized hardware and software resources
- Lack of space

### Characteristics of virtualization environments

You have physical resources on the bottom and a software layer called virtualization layer, virtualization manager or hypervisor. 
Essentially emulate the behaviour of physical resources. Hypervisor gives you the possibility of create virtual hardware so to allocate portion of CPU or portion of network interface card to a specify user, the same can be done with storage and other kind of resources.

#### The essential characteristics of virtual environments

    - Increased security
    - Managed execution
    - Portability

#### Increased Security

The virtual machine manager control and filter the activity of the guest, thus preventing some harmful operations from being performed.
    - **Resources** (or sensitive information) exposed by the host **can be hidden** or simply protected from the guest.
    - Example you can test software downloaded from internet, in the vm as sandbox for Java. The sandbox filters harmful instructions on the bases of security policies.

#### Managed execution

Emulation can be used to run legacy application developed for hw architecture that today are not producted as very old mainframe.

Sharing, aggregation, emulation, isolation (virtualization)

#### Portability

A Java or Python code can be executed on any environment running a JVM or Python VM.
A VM is booted rom a VM image that is stored in specific format (there is no standard)
> vdi, vhd, vhdx, vmdk, ami, ari, aki, ...

- A Docker container can run on any platform running the Docker engine.

#### Operating system level Virtualization

Comparison of classical stack for system level virtualization (we have hypervisor) vs operating system virtualization (container).

**System level virtualization**
We have the Host OS and in his top we have hypervisor and then we have different VMs with Guest OS (each one with his bins/libs).

**Operating system virtualization (container)**
With containers we remove this two layers of OS, we remove hypervisor layer.
Bins/Libs are shared among all applications, which is managed by the container manager.
We lose something in *Isolation*
Maybe for the Application A is good run over the host os with app B and maybe for the Application B cannot be good run on the same OS where run app A.

![VM vs Container](./images/OS_system_level_virtualization.PNG)


Container are based essential over 3 technologies
- Namespace - what you can see and use
- Cgroups - how much you can used
- unionFS (File System) - allows to pack together different component of applications

##### Container (cont'd)

Is an evolution of ***chroot*** mechanism (unix fs)
- the chroot operationg changes the root directory for a process and all childrens of that process
    - For this process, this is the root file system, this proc and all childrens can use only this FS and they can't change nothing outside of it.

#### Namespace

"**what you can see and use**"

The idea of namespace is to wrap a set of resources in an abstraction and **define who can access to that specific resources**. Given a process we can assign to this process a PID (process id) we can assign a set of user and user group we can assign network device.<br>
All resources available in a operating system can be partially assign to a process using the mechanism of namespace. <br>
When a process is created, one or more namespaces are created for this process

One process cannot interact directly to another namespace of another process.

#### Cgroups

"**how much you can used**"

Cgroups alias Control groups
are a Linux Kernel feature which allow processes to be organized into hierarchical groups whose **usage of various types of resources** can then be limited and monitored.

cgroups can be splitted in other cgroups

#### unionFS

"**allows to pack together different component of applications**"

UnionFS used by Container.
Is a FS is created to merge to different kind of FS and give a unified vision of different FS that are avaiable. files are separated byt merged them logically into a single view.

Union: a collection of merged directories; each physical directory is called a branch

The unionFS intercept the call to the specific FS and translate the call into the specific FS.

![unionFS](./images/unionFS.png)

##### Union FS (cont'd)

```bash

ls /Fruits
    Apple Tomato
ls /Vegetables
    Carrots Tomato
cat /Fruits/Tomato
    I am botanically a fruits.
cat /Vegetables/Tomato
    I am horticulturally a vegetable.

mount -t unionfs -o dirs=/Fruits:/Vegetables > none /mnt/healthy

ls /mnt/healthy
    Apple Carrots Tomato
cat /mnt/healthy/Tomato
I am botanically a fruits.
```

here Fruits has higher priority than Vegetables
there are two file that have the same filename, the file are unified the file considered is one in the Fruits dir because has higher prioriority than Vegetables.

##### unionFS Copy-on-Write Unions

- Unionfs also can mix read-only and read-write branches (fs).

If we have a FS read-only the unionFS show it as RW FS. Copy-on-Write semantics to give the illusion that you can modify files and directories on read-only branches. When the Read only FS will be unmounted we lose all modification on the FS.

### Process virtualization with Docker container

Docker is this virtualization manager is the standard de facto for container.
- Containers run directly within the host machine's kernel
    - containers + bare metal
    - containers + virtual machine

We can have two configure, bare metal or vm.
**Bare metal**: the container runs directly on the bare metal:
    - physical machine, OS, Containers

**VM**: or for easy management of a cloud info, we can have a vm and on top of the vm we have the containers

This second solution is larger used by cloud provider, because the cloud provider first build their infostructure on vm and then on containers. So it's more easy to manage container on that way

#### Docker engine

You can use REST API to communicate to the docker engine.
Or you can use the CLI (Command Line interface) which call the REST API to interact with the docker engine.
The daemon create and manage docker objects.

You can manage the network, storage, containers, images.

## Docker Architecture

![Docker Architecture](./images/dockerArchitecture.PNG)

Images: Are read only template with instruction for creating container.
Containers are R/W

## Example of a docker CLI Command

```sh

docker run -i -t ubuntu /bin/bash

```

The Docker deamon does what follow:
- Locate and **eventually download the ubuntu image** (if not locally stored)
- Create a **new containers**
- **Allocate** a R/W **file system** as final layer of the image
- **Create a network interface** connected to the default networking
- Start the container and **run /bin/bash command**

## Layer concept

![Layer concept](./images/layer_concept.png)

A container is build by layers, each one contains something.
For example in the image you can see, the OS (ubuntu), the code (app) and command to be executed (for ex *make*) for that specific application.

That container is packed together, with the feature of unionFS, which allows to pack different FS. Each block is itself a container that is packed together

The structure of the containers is described from the **Dockerfile** is build starting to this specific Ubuntu image, then you specify you want copy this file in the local fs in the container then you specify to build the application with the command *make* then the build of the app is stored into a layer on top.

## Container - Storage options

- Container writable layer
- Bind mount
- Volumes
- tmpfs mount

**Container writable layer** is useful when we have to process data at runtime.

Other *persistent* solution, **bind mount** and **volumes** you can use it to store permanently data which can access by the containers (and host).

**tmpfs mount** (Temporary File System), works only on Linux, temporary file system.

### Container writable layer - not persistent

- Does **not persist** when the container is terminated.
- **Reduce portability**: writable layer is tightly coupled with the host.
- **Reduced performance**: slower than other storage solution.

### Bind Mount - Persistent

Is persistent, you can mount any file or directory on the host machine into the container.
Each time the container, write/modify/delete a file in this folder it modify the files directly into the host FS.

*Portability*:The solution is not portable, if you want to move the container into another FS which differ by the structure you can have some problem. Because depend by a specific path.

*Performance*: The performance are higher than Volumes.

### Volume - Persisten

With Volume you have an area of storage isolated from the host and it's created e managed by the docker. Stored within a directory on Docker host.
You can mount it in R/W or R.
*Portability*: Volumes works on both on Linux and Windows containers.
Volumes are to easy to backup and migrate. Volumes doesn't depend by a path in the host fs like in bind mount.

### tmpfs - not persisent

- An area of memory outside the container writable layers.

- Is temporary and only persised in the host memory.
    - Removed when the container stop.

- Limitations:
    - Available only in Linux hosts.
    - Not shareble among containers.

The advantages is in memory fs, and faster than in disk fs.

## Best cases for using storage options:

- **Volumes**
    - Should always used when we want store persistent data
    - Best when you want share data among multiple containers.
    - Move the containers to different hosts and you cannot do any assumption on other FS.
    - When you need to do backup or migrate.
- **Bind Mount**
    - If you want share configuration file, source code from host to containers
    - When you are sure the containers run on a specific host, and the hosts have the same file system and directory structure.
- **tmpfs mounts**
    - When you do not want the data to persist either on the host machine or within the container
    - For security reasons.
    - To guarantee performance of the container when your applications need to write a large volume of non-persisent state data.

## Docker Services

We can use a single docker containers for each micro services.
So we need some mechanism to link together this containers.

### Swarm

- Another case is when we have an application with the need to scale, like a web server, and we want orchestrate this containers all together.
- An abstraction that allows to scale containers in a Swarm
- A swarm is a set of docker deamons (managers) and containers (workers)
    - in the simplest deployment: 1 manager (one single machine), 1 or more workers


### Docker Compose

- A docker services allows to define
    - How many container should be Available
    - Constraints on the use of Resources
    - Load balancing
    - Recovery procedures

The services are described by a docker-compose.yml file.

![docker compose](./images/docker-compose.png)

Deploy: We can specify the kind of deployment, here in the image we can see 5 replicas with the use of 0.1 of cpu and maximum 50mb of memory.
Restart-policy: If one of this replica file we have to restart the replica.

## Networking

- You can connect Container and Service together, or connect them to non-Docker workloads
- Type of network drivers
    - Bridge
    - Host
    - Overlay
    - Macvlan
    - None: if you don't need networking

- **Bridge**
    - The default network drivers
    - is usually used when your applications run in standalone containers that need to communiticate
- **Host**
    - For standalone containers
    - Use the host's networking directly (Remove the network isolation)
    - Host is allowed only for swarm services on Docker 17.06 or higher.
- **Overlay**
    - Overlay used to connect multiple Docker daemons together and enable swarm services to communicate with each other.
    - facilitate communication between a swarm service and standalone container or between two standalone containers.
    - Overlay (layer) allows to remove the need to do OS-level routing between these containers.
- **macvlan**
    - macvlan nets allow you to assign a MAC address to a container, making it appear as a physical device on your network.
    - The docker deamon routes traffic to containers by their MAC addr.
    - Sometimes is the best choice when dealing with legacy app that expect to be directly connected to the physical network.
![summary networking drivers](./images/summary_networking_drivers.PNG)


