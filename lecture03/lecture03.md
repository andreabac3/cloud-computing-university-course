# Enabling technologies

One of the component is enabling technologies all tecnhologies allows to deploy cloud system.

Most of this technologies are from the Distributed Computing.


## Distributed Computing

Is a computational models
Define how computation is carried on.
The computation is broken down into computational units executed concurrently on different computing elements.
-  Computing elements could be different processor (on different nodes) of the same physical or virtual machine and so one.

Computing elements could have different location with a heterogeinity of hardware and software resources.

### Example of distributed computation

We have a large text and we want compute the word count with the MapReduce model.

![wordCountMapReduce.png](./images/wordCountMapReduce.png)

We can split the file into smaller files and counting the word in parallel and then we merge the results.

![distributedStorage.png](./images/distributedStorage.png)

