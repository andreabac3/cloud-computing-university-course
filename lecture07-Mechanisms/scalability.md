# Scalability

**Perfomance** measures how fast fast and efficelty a sw can complete a certain task.

**Scalaibility** measures the tren of perfomance with increasing load, what it happening when the load increase.

> A system is defined scalable if mantain the perfomance despite of the increase of load

**Linear scalabilty** as much increas the load as much add new resources.

**Non linear**, when we reach one point to handle a small portion of load we need a huge number of resources

## Autoscaling mechanisms 

Course grain - replication of an application

- Typically involves the delpoyment of a new VM.
- With container could be also the replication of a container or a set of containers (pod) inside a VM.

Fine grain - replication of an app component (micro-services)

- Micro services is a SW architecture that decompose and app in independent  components
- Running a micro service in a container makes it possible to replicate the hotspot components of an app

### Automated scaling listener

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides11.png" alt="slides11" style="zoom:50%;" />

To do the scaling usually are predefined algorithms, and the decision taken either  workload counters in term of a request or perfomance counters (i.e. CPU utilisation)

The goal auto scaling is to "predict" how many resources should be added.

The resources could be implemented either horizontal scaling ( replication of the resources) or vertical scaling ( increase or decrease of the resources capacities)

The summaries of the all pipeline, this is called 

**MONITOR-ANALYZE-PLAN-EXECUTE**<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides12.png" alt="slides11" style="zoom:50%;" /> 

 ### Autoscaling algorithms

- Threshold based- reactive, threshold on wordload or resource utilization, when the threshold is exceed the scaling action is triggered 
- Molde based, a math model of the system, the solution of the model give the scaling action to be performed, triggered by events, with can find a exact solution or an approximate solution.

- Proactive, we can observe the workload and we can predict the value of workload counter in the short term, then using this values as an input of thereshold base approach or a model base and then we can determine in advance how many resources we need.

## AWS Autoscaling

- Autoscaling group, a collection of EC2 instances that share similar characteristics

- Launch configuration, a template that an Auto Scaling group uses to launch EC2 istances
- Scaling plan, scaled based on demand

When we define an autoscaling group the associated an launch conf and a scaling plan

We have an EC2 instances which is attach to a pending, when a scale out events happen we go from pending state from pending wait, pending proceed and finally to the in service. When the instances is in service it could happen terminate or in a stand by mode, in stand by mode the instance could return tu pending withouth pass thorugh pending wait and proceed, the instance in ready to go directly to the in service. In the service mode, the instance could be detach to the autoscaling group

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides13.png" alt="slides13" style="zoom:50%;" />



<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides14.png" alt="slides13" style="zoom:50%;" />

Simple scaling is a **threshold policy**

Target scaling is model based

### Threshold policy

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides15.png" alt="slides13" style="zoom:50%;" />

Type of adjustment;

- change of capacity, the number of VM;  you specify the capacity, i.e. if the current capacity is 3 and adjustment is 5, then when this policy is performed, Autoscaling adds 5 instaces to the group for a total of 8 instances.
- new capacity, you specify the new capacity of the scaling group, if the group is 3 istances and the adjustment is 5, the final capacity is of 5 instances
- percent capacity, you specify the percentage on increment/decrement of capacity

> You should specify the max or the min scaling group size

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides16.png" alt="slides13" style="zoom:50%;" />

If the CPU utilization is between 50 and 60 doesn't add anything, up to 60 till 70 add 10% and so on and so forth.

MANCANO DUE SLIDES (40-45 secondo video)

**We have scale out policy to add resources, and scale in to remove resources** 

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides17.png" alt="slides13" style="zoom:50%;" />

MAPE-K cycle, 

- the analyze phase is done by checking the allarm defined, it's role of the CW

- Dynamic scaling, is done by one of the scaling mechanism
- Execute, using API or CLI
- Knowledge made by the collected metric  

## Autoscale container

Orchestator allow us to manage autoscaling in a container environment, one of them is K8S and handle container scheduling.

Swarm allows to maintain a certain capacity (number of container) but is manually done, whereas K8S handle it an automatic way

### K8S architecture

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides18.png" alt="slides13" style="zoom:50%;" />

It's a master-slave architecture, nodes are workers and master take decision.

K8S doesn't have monitor service, you need to install

<img src="/Users/riccardo/OneDrive - uniroma1.it/uni/master/cloud-computing/cloud-computing-university-course/lecture07-Mechanisms/img/slides20.png" alt="slides13" style="zoom:50%;" />

Pod a set of a container

### Avaibility zones

The servers that are into datacenter are splitted into avaibability zone, to deal with possible fail. Each datacenter is a region, when we decide to deploy distrubuted app, we could decide where to run app both decide either in single region or in different regions