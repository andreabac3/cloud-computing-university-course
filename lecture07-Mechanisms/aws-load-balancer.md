# Load balancer

The idea is to distribute the work load trough many instances, there are different policies, classified in 4 category:

- Content-blind distribution, request are distributed in a pseudo-uniform way (i.e. round robin)
- Asymmetric distribution, usually you leverage the fact that you collect computing resources with differenct capacities, you can assign more load to powerful resources. If you have homogeneous it does make sense;
- Workload priorazitation, is assigned a priori to the workload, you can have different cateogry of priority of users and based on that, you apply differnt worload techniques;
- Contet-Aware distirbution, it's based on the content of the request, assign it in a way that the workload is balanced  

### Layer 4 (network level) and later 7 (application level)

They are two kind of load balancer

<img src="img/slides06.png" alt="slides1" style="zoom:50%;" />

- Layer 4, is stateless when there is a request from the client, the l.4. works as proxy, all the massages are forwarded to the target web server i.e. two request sent by the same client could be handled with two different web server, according to the load balanced policy

- Layer 7 is a stateful load balancing, the load balancer keeps the trak of the client and serve it with the same web server

<img src="img/slides07.png" alt="slides1" style="zoom:50%;" />

- Two way, all the incoming and outgoing request go through the load balancer, here we have a bottleneck, the load balanced should be very powerful in order to satisfy the amount of request. Easy to implement
- One way, just the incoming req goes trhough the load balanced, whereas the outcoming req. goes directed to the client. Hard to implement

### AWS load balanced

It has both: layer 4 and layer 7

### Layer 4 

<img src="img/slides08.png" alt="slides1" style="zoom:50%;" />

For instances for the same protocol (i.e. HTTP) we could hanlde it with differe nt listener, so every listener has assigned a specific protocol and port and has many rules which manage the request accoriding  a specific policy

Target is the VM which recive the traffic identify by id or IP, each of targets is grouped with a target groups, that define a set of target.

### Layer 7

<img src="img/slides09.png" alt="slides1" style="zoom:50%;" />

At the app level we have the same organizzation, listener rule could be more sophisticated, to each rule is sent a priority.

Support the sitcky session, you can redirect the req. belonging into the same session (client) to the same target; implement with cookies. 

<img src="img/slides10.png" alt="slides1" style="zoom:50%;" />