## Cloud usage monitor

### Monitor agents

<img src="img/slides01.png" alt="slides1" style="zoom:50%;" />

All the traffic goes trough the monitor ag. and it collects several data such as # of buckets # of messages, the size of the message, however the monitor ag. can go inside the speicifc packet. All information collected are stored in a log database in order to analyse them in the future.

Cloud consumers -> Monitor agent -> Cloud Service, this pipeline is called one way mechanism

### Resource agent 

<img src="img/slides02.png" alt="slides1" style="zoom:50%;" />It retrives all information in active way, stated differently is event-driven: when an event occur, the resource agent collect metrics in the log database, it has an amount of time which drives it to stored the information, every amount of time continuosly stores data. In general when a specific event is fired, the resource handles it.

Kind of event:

- Usage event, i.e. CPU consuming;
- time event;

- Scaling event

## Polling agents

<img src="img/slides03.png" alt="slides1" style="zoom:75%;" />

It, in the polling mechanism we have a retrival approach with a speicific period, it`s send a message to the cloud service to get the information and then it stores info to the db. We can either store all the values or just the last changes.

## AWS CloudWatch

It's the most important AWS component, in theory is based on a push model, implemented with a publish/subscribe machanism.

The idea is that when you run CW you subrisce it to a specific data source (i.e. utilization of CPU , net trafic...)

**For each resources available in AWS you can access to a set of metrics which you can attach CW in order to log them.**  

You can set an allarm togheter a metric, which checks if a specific condition holds. 

### Main CW concepts

**Namespace**, is a container (in the sense of bucket/ box) for metrics (don,t confuse it with docker or linux meanin) is a set of metrics for a specific resource. 

A **metric** is time oriented set of  datapoints, it has:

- unique id;
- metrics expires after a period between 3h-15 months (**can't be manually deleted**)
- each data point is marked with time stamp.

For a given namespace and a metric we have a pair **name/value**, the same metric can be labeled with different value of dimension, every pair is treated as different metric.

![slides1](img/slides04.png)

Let Server=Beta, we can see the same metric (Unit count) but the different value.

## Statistic

For each datapoints in the same set (the same metrics), CW collects min, max,std etc.. This measure are computed every amount of time(60 sec.)

## Allarm

You can match an allarm to a specific action, when it's fired the allarm performs a specific task.  For further info see slide below. 

<img src="img/slides05.png" alt="slides1" style="zoom:25%;" />

### Simple notification servies

When an allarm fires, there is a service which send notification for all allarm's subriscreber 

## Pay monitoring

An important purpose about CW is to handle the credit consume for the used resources, its name is **pay-per-use monitor**. **WE CANNOT DO ANY CONFIGURATION FOR THAT MONITOR, WE COULD JUST DEFINE AN ALLARM FOR IT, AND USING A POLICY TEST WHAT IS THE FUTURE COST AMOUNT FOR THE CUSTOMER USAGE**